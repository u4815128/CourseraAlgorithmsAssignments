package Week2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Solution to week 2 homework of Coursera Algorithms: Design & Analysis
 * Implement QuickSort, experimenting with choosing the pivot from different locations in the array and checking
 * time complexity
 */
public class QuickSort {

    static int comparisonsDone = 0;

    /**
     * Read in the text file of unsorted ints from 1-10000
     * @return a 10000-element array of unsorted chronological ints
     */
    private static int[] importData() {
        int[] data = new int[10000];

        // Read in file
        try {
            Scanner s = new Scanner(new File("src/Week2/QuickSort.txt"));
            int index = 0;

            while (s.hasNext()) {
                int nextInt = Integer.parseInt(s.next());
                data[index++] = nextInt;
            }
            s.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static int quicksortFirst(int[] nums, int start, int end) {
        if (start < end) {
            int pivot = partitionFirst(nums, start, end);
            quicksortFirst(nums, start, pivot - 1);
            quicksortFirst(nums, pivot + 1, end);
        }
        return comparisonsDone;
    }
    private static int partitionFirst(int[] nums, int start, int end) {
        int pivot = nums[start];
        // initiate var to maintain the partition, keep <= pivot on left
        int lastSmallerThanPivot = start;
        // traverse array and check if each element is less than or equal to the pivot
        for (int j = start + 1; j <= end; j++) {
            if (nums[j] <= pivot) {
                // increment lastSmallerThanPivot to move partition right by one
                lastSmallerThanPivot++;
                // swap new <= pivot number to correct location
                int toMove = nums[lastSmallerThanPivot];
                nums[lastSmallerThanPivot] = nums[j];
                nums[j] = toMove;
            }
        }
        // Finally, swap pivot to correct location
        int lastSmaller = nums[lastSmallerThanPivot];
        nums[lastSmallerThanPivot] = pivot;
        nums[start] = lastSmaller;
        // Count number of comparisons conducted
        comparisonsDone += (end - start);
        return lastSmallerThanPivot;
    }

    public static void main(String[] args) {

        int[] data = importData();
        int compsDone = quicksortFirst(data, 0, 9999);
        comparisonsDone = 0;
        System.out.println(compsDone);


    }
}
